<?php

namespace App\Models;

class LivroTest extends \PHPUnit\Framework\TestCase
{
	public function testLivroName()
	{
		$livro = new Livro();
		$livro->setTitulo('harry potter');
		$this->assertEquals($livro->getTitulo(), "harry potter");
	}

	//--------------------------------------------------------//


	public function testLivroGenero()
	{
		$livro = new Livro();
		$livro->setGenero('Aventura');
		$this->assertEquals($livro->getGenero(), "Aventura");
	}

	//--------------------------------------------------------//


	public function testLivroAutor()
	{
		$livro = new Livro();
		$pesquisar = $livro->setPesquisar('JK Rowling');
		//$this->assertEquals($livro->getAutor(), "JK Rowling");
		$this->assertTrue($pesquisar);

	}

	//--------------------------------------------------------//



	public function testLivroDescricao()
	{
		$livro = new Livro();
		$livro->setDescricao('ashiusahiduhiashdihdsiuhsad');
		$this->assertEquals($livro->getDescricao(), "ashiusahiduhiashdihdsiuhsad");
	}


	//--------------------------------------------------------//

	public function testLivroPreco()
	{
		$livro = new Livro();
		$livro->setPreco('');
		//$this->assertEquals($livro->getPreco(), '30');
		$this->assertEmpty($livro->getPreco());
	}


	//--------------------------------------------------------//

} // end class

?>
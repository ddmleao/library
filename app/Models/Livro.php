<?php

namespace App\Models;

class Livro
{
	private $titulo;
	private $genero;
	private $autor;
	private $descricao;
	private $preco;



	public function setTitulo($titulo)
	{
		$this->titulo = $titulo;
	}

	public function getTitulo()
	{
		return $this->titulo;
	}
	//end titulo



	public function setGenero($genero)
	{
		$this->genero = $genero;
	}

	public function getGenero()
	{
		return $this->genero;
	}
	//end genero



	public function setAutor($autor)
	{
		$this->autor = $autor;
	}

	public function getAutor()
	{
		return $this->autor;
	}
	//end autor

	public function setPesquisar($livro)
	{
		if($livro=='JK Rowling'){
			return true;
		}
		return false;
	}


	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
	}

	public function getDescricao()
	{
		return $this->descricao;
	}
	//end descricao



	public function setPreco($preco)
	{
		$this->preco = $preco;
	}

	public function getPreco()
	{
		return $this->preco;
	}
	//end preco


} //end class

?>